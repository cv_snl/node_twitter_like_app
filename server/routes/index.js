const express = require('express');
const passport = require('passport');
const gravatar = require('gravatar');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express from server folder' });
});

/* GET login page. */
router.get('/login', function(req, res, next) {
  res.render('login', { 
    title: 'Login Page', 
    message: req.flash('loginMessage') 
  });
});

/* GET Signup */
router.get('/signup', function(req, res) {
  res.render('signup', { 
    title: 'Signup Page',
    message: req.flash('signupMessage') 
  });
});

/* GET Profile page. */
router.get('/profile', isLoggedIn, function(req, res, next) {
  res.render('profile', { 
    title: 'Profile Page', 
    user : req.user,
    avatar: gravatar.url(req.user.email , {s: '100', r: 'x', d:'retro'}, true) 
  });
});

/* GET Logout Page */
router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

/* POST login */
router.post('/login', passport.authenticate('local-login', {
  //Success go to Profile Page / Fail go to login page
  successRedirect : '/profile',
  failureRedirect : '/login',
  failureFlash : true
}));

/* POST Signup */
router.post('/signup', passport.authenticate('local-signup', {
  //Success go to Profile Page / Fail go to Signup page
  successRedirect : '/profile',
  failureRedirect : '/signup',
  failureFlash : true
}));


// HELPERS
/* check if user is logged in */
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated())
    return next();

  res.redirect('/login');
}
module.exports = router;
