module.exports = app => {
  // ODM with Mongoose
  const mongoose = require('mongoose');
  // Modules to store session
  const session = require('express-session');
  const MongoStore = require('connect-mongo')(session);
  // Database configuration
  const config = require('./config.js');
  // connect to our database
  mongoose.connect(config.url);
  // Check if MongoDB is running
  mongoose.connection.on('error', () => {
    console.error('MongoDB Connection Error. Make sure MongoDB is running.');
  });
  
  // required for passport
  // secret for session
  app.use(session({
    secret: 'sometextgohere',
    saveUninitialized: true,
    resave: true,
    //store session on MongoDB using express-session connect mongo
    store: new MongoStore({
      url: config.url,
      collection : 'sessions'
    })
  }));
}